package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {


	private static final String GET_ALL_USERS = "SELECT * FROM users";
	private static final String ADD_USER = "INSERT INTO users (login) VALUES (?)";
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
	private static final String GET_ALL_TEAMS = "SELECT * FROM teams";
	private static final String ADD_TEAM = "INSERT INTO teams (name) VALUES (?)";
	private static final String GET_USER_TEAMS = "SELECT * FROM teams WHERE id IN(SELECT team_id FROM users_teams WHERE user_id = ?)";
	private static final String ADD_USER_TEAMS = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
	private static final String GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
	private static final String GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";
	private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id =?";
	private static final String DELETE_USER = "DELETE FROM users WHERE login = ?";
	private static final String CONNECTION_URL_PROPERTY_NAME = "connection.url";
	private static final String CONNECTION_PROPERTIES_FILE_NAME = "app.properties";
	private final String url;

	public DBManager() {

		Properties properties = new Properties();
		try (FileReader reader = new FileReader(CONNECTION_PROPERTIES_FILE_NAME)) {
			properties.load(reader);
			url = properties.getProperty(CONNECTION_URL_PROPERTY_NAME);
		} catch (IOException e) {
			e.printStackTrace(); // log
			throw new IllegalStateException(e);
		}

	}


	public static synchronized DBManager getInstance() {
		return new DBManager();
	}

	private Connection getConnection(boolean autocommit) throws SQLException {
		Connection con;
		con = DriverManager.getConnection(url);
		con.setAutoCommit(autocommit);
		return con;
	}

	private User mapUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setLogin(rs.getString("login"));
		user.setId(rs.getInt("id"));
		return user;
	}


	public List<User> findAllUsers() throws DBException {
		try (Connection con = getConnection(true);
			 Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(GET_ALL_USERS)) {
			List<User> users = new ArrayList<>();
			while (rs.next()) {
				User user = mapUser(rs);
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't get user", e);
		}
	}


	public boolean insertUser(User user) {
		Connection con = null;
		try {
			con = getConnection(false);
			addUser(con, user);
			con.commit();
			return true;
		} catch (SQLException e) {
			rollback(con);
			return false;
		} finally {
			close(con);
		}
	}

	private static void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void addUser(Connection con, User user) throws SQLException {
		try (PreparedStatement statement = con.prepareStatement(ADD_USER, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			statement.setString(++i, user.getLogin());
			int c = statement.executeUpdate();
			if (c > 0) {
				try (ResultSet keys = statement.getGeneratedKeys()) {
					if (keys.next()) {
						user.setId(keys.getInt(1));
					}
				}
			}
		}
	}


	public boolean deleteTeam(Team team) {
		Connection con = null;
		try {
			con = getConnection(false);
			PreparedStatement stmt = con.prepareStatement(DELETE_TEAM);
			stmt.setString(1, team.getName());
			stmt.executeUpdate();
			con.commit();
			return true;
		} catch (SQLException e) {
			rollback(con);
			return false;
		} finally {
			close(con);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
			Connection connection = null;
			try {
				connection = getConnection(false);
				for(User user: users)
					deleteUser(connection,user);
				connection.commit();
				return true;
			} catch (SQLException e){
				rollback(connection);
				throw new DBException("Can't delete user", e);
			}
	}

	private void deleteUser(Connection connection, User user) throws SQLException {
			PreparedStatement ps = connection.prepareStatement(DELETE_USER);
			ps.setString(1, user.getLogin());
			ps.executeUpdate();

	}

	public User getUser(String login) throws DBException {
		try (Connection con = getConnection(true);
			 PreparedStatement ps = con.prepareStatement(GET_USER_BY_LOGIN)) {
			ps.setString(1, login);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return mapUser(rs);
		} catch (SQLException e) {
			throw new DBException("Can't get user", e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try (Connection con = getConnection(true);
			 PreparedStatement ps = con.prepareStatement(GET_TEAM_BY_NAME)) {
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return mapTeam(rs);
		} catch (SQLException e) {
			throw new DBException("Can't get team", e);
		}
	}


	public List<Team> findAllTeams() throws DBException {
		try (Connection con = getConnection(true);
			 Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(GET_ALL_TEAMS)) {
			List<Team> teams = new ArrayList<>();
			while (rs.next()) {
				Team team = mapTeam(rs);
				teams.add(team);
			}
			return teams;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't get team", e);
		}
	}

	private Team mapTeam(ResultSet rs) throws SQLException {
		Team team = new Team();
		team.setName(rs.getString("name"));
		team.setId(rs.getInt("id"));
		return team;
	}

	private void addTeam(Connection con, Team team) throws SQLException {
		try (PreparedStatement statement = con.prepareStatement(ADD_TEAM, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			statement.setString(++i, team.getName());
			int c = statement.executeUpdate();
			if (c > 0) {
				try (ResultSet keys = statement.getGeneratedKeys()) {
					if (keys.next()) {
						team.setId(keys.getInt(1));
					}
				}
			}
		}
	}

	public boolean insertTeam(Team team) {
		Connection con = null;
		try {
			con = getConnection(false);
			addTeam(con, team);
			con.commit();
			return true;
		} catch (SQLException e) {
			rollback(con);
			return false;
		} finally {
			close(con);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try {
			con = getConnection(false);
			PreparedStatement stmt = con.prepareStatement(ADD_USER_TEAMS);
			for (Team team : teams) {
				stmt.setInt(1, user.getId());
				stmt.setInt(2, team.getId());
				stmt.executeUpdate();
			}
			con.commit();
			return true;
		} catch (SQLException e) {
			rollback(con);
			throw new DBException("Can't set teams for user", e);
		} finally {
			close(con);

		}
	}


	public List<Team> getUserTeams(User user) throws DBException {
		try (Connection con = getConnection(true);
			 PreparedStatement ps = con.prepareStatement(GET_USER_TEAMS)) {
			ps.setInt(1, user.getId());
			ResultSet rs = ps.executeQuery();
			List<Team> teams = new ArrayList<>();
			while (rs.next()) {
				Team team = mapTeam(rs);
				teams.add(team);
			}
			return teams;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Can't get team", e);
		}
	}


	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		try {
			connection = getConnection(false);
			PreparedStatement statement = connection.prepareStatement(UPDATE_TEAM);
			int i = 0;
			statement.setString(++i,team.getName());
			statement.setInt(++i,team.getId());

			statement.executeUpdate();
			connection.commit();
			return true;
		}catch (SQLException e){
			rollback(connection);
			throw new DBException("Can't update Team",e);
		} finally {
			close(connection);
		}
	}
}

